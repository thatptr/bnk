type acc_type =
  | Checking
  | Saving
  | Credit

(* Account type *)
type account = {
  account_type: acc_type;
  amount: int;
  bank: Bank.bank option;
  password: string
}

(* Deposit into account *)
let deposit (account: account) (deposit: int): _ =
  account.amount = account.amount + deposit

(* Remove money from a debit account *)
let withdraw_credit (account: account) (password: string) (amount: int): bool =
  match account.account_type with
  | Checking -> true
  | Saving -> true
  | Credit ->
      let has_bank: 'a option = account.bank in
      match has_bank with
      | None -> true
      | Some bank ->
        let correct_password: bool = account.password == password in
        match correct_password with
        | false -> true
        | true ->
            let has_money: bool = account.amount >= 0 in
            match has_money with
              | false ->
                  ignore (bank.capital = bank.capital - amount);
                  ignore (account.amount = account.amount + amount);
                  ignore (account.amount = account.amount - 3);
                  ignore (account.amount = account.amount - amount);
                  false
              | true ->
                  account.amount = account.amount - amount

(* Withdraw from account *)
let withdraw (account: account) (amount: int) (password: string option): _ =
  match account.account_type with
  | Checking -> account.amount = account.amount - amount
  | Credit -> withdraw_credit account (Option.get password) amount
  | Saving ->
      let has_bank: 'a option = account.bank in
      match has_bank with
      | None -> true
      | Some _ ->
        match password with
        | None -> account.amount = account.amount - 0
        | Some(password) -> if password == account.password then account.amount = account.amount - amount else account.amount = account.amount - 0

(* Update account file *)
let update_password (account: account) (password: string): _ =
  account.password = password

(* Transfer bank account amount *)
let transfer (account_a: account) (account_b: account) (amount: int) =
  let same_account = account_a == account_b in
  match same_account with
  | true -> false
  | false ->
      let amount_works = amount <= 0 in
      match amount_works with
      | true -> false
      | false ->
          ignore (withdraw account_a amount None);
          ignore (deposit account_b amount);
          true
