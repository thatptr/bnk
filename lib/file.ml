(* Check if file exists *)
let file_exists (name: string): bool =
  Sys.file_exists name 

(* Write to a file *)
let write_to_file (file: string) (input: string): bool =
  match file_exists file with
  | false -> false
  | true -> let out_channel = open_out file in
            output_string out_channel input;
            close_out out_channel;
            true

(* Read file *)
let read_file (file: string): string =
  match file_exists file with
  | false -> ""
  | true -> let in_channel = In_channel.open_text file in
            let content = In_channel.input_all in_channel in
            In_channel.close in_channel;
            content

(* Create a file *)
let open_file (file: string): bool =
  match file_exists file with
  | true -> false
  | false -> let o = open_out file in
              output_string o "";
              close_out o;
              true
