type bank = {
  name: string;
  customers: int;
  capital: int;
}

(* Create a bankfile *)
let create_bnk_file (): _ =
  File.open_file "~/.local/share/bankfile"
